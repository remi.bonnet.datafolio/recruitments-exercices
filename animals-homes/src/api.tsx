interface Animal {
    animal_id: number
    home_id: number
    animal_name: string
}

interface Home {
    home_id: number
    home_name: string
}

export function getHomes(): Promise<Home[]> {
    return Promise.resolve([
        {"home_id": 42, "home_name": "My Home"},
        {"home_id": 43, "home_name": "Neighbor's Home"},
        {"home_id": 44, "home_name": "Mom's Home"},
    ])
}

export function getAnimals(): Promise<Animal[]> {
    return Promise.resolve([
        {"home_id": 42, "animal_id": 1, "animal_name": "Kiwi"},
        {"home_id": 42, "animal_id": 2, "animal_name": "Paws"},
        {"home_id": 44, "animal_id": 3, "animal_name": "Flower"},
    ])
}
